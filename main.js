new Vue({
    el: '#app',
    data: {
        ProjectName: 'Vote!',
        voteInformation: submissions,
    },
    methods: {
        printInformation: function() {
            // console.log(this.voteInformation[3].id)
        },
        upVote: function(ID) {
            var submission = this.voteInformation.find(s => s.id === ID);
            submission.votes++;
        },


    },
    computed: {
        sortedSubmissions() {
            return this.voteInformation.sort((a, b) => {
                return b.votes - a.votes
            });
        }

    } // copu
})